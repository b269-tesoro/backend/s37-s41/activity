const User = require("../models/User");
const Course = require("../models/Course");

// "bcrypt" is a pw-hashing function that is commonly used in computer systems to store user pw securely
const bcrypt = require("bcrypt");
/*
Business Logic:
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the Postman application based on the result of the "find" method
*/

const auth = require("../auth");

// Check if email alr exists
module.exports.checkEmailExists =  (reqBody) =>
{
	return User.find({email: reqBody.email}).then(result => 
	{
		if(result.length > 0)
		{
			return true;
		}
		else
		{
			return false;
		}	
	});
};

// User Registration
/*
BUSINESS LOGIC
1. Create a new User object using the mongoose model and the information from the request body
2. Make sure that the password is encrypted
3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) =>
{
	let newUser = new User
	({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// bcrypt.hashSync() is a function in the bcrypt library used to generate hash value for a given input string snychronously
		// first argument is the input string, second argument is number of hash iterations called "salt rounds"
		password: bcrypt.hashSync(reqBody.password, 10) 
	});

	return newUser.save().then((user, error) =>
	{
		if (error) 
		{
			return false;
		}
		else
		{
			return true;
		}
	});
}

// User Authentication
/*
Business Logic:
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) =>
{
	return User.findOne({email: reqBody.email}).then(result =>
	{
		if (result == null)
		{
			return false;
		}
		else
		{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect)
			{
				return {access: auth.createAccessToken(result)};
			}
			else
			{
				return false;
			}
		}
	});
};

// S38 ACTIVITY START
/*
Business Logic:
1. Find the document in the database using the user's ID
2. Reassign the password of the returned document to an empty string
3. Return the result back to the frontend
*/

module.exports.getProfile = (data) =>
{
	return User.findById(data.userId).then(result =>
	{
		result.password = "";

		return result;
	});
};
// S38 ACTIVITY END

// Authenticated user enrollment
/*
Business Logic:
1. Find the document in the database using the user's ID
2. Add the course ID to the user's enrollment array
3. Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async(data) =>
{
	// Add Course ID in the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then(user =>
	{
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) =>
		{
			if (error) {return false;}
			else {return true;}
		});
	});

	// Add User ID in the enrollees array of the course
	let isCourseUpdated = await Course.findById(data.courseId).then(course =>
	{
		course.enrollees.push({userId: data.userId});
		return course.save().then((course,error) =>
		{
			if (error) {return false;}
			else {return true;}
		});
	});

	if(isUserUpdated && isCourseUpdated)
	{
		return true;
	}
	else
	{
		return false;
	}
};
