// set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// allows us to use all the routes defined in userRoute.js
const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");


// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// allows all user routes created in "userRoute.js" file to use "/users" as route
app.use("/users", userRoute);
app.use("/courses", courseRoute);

// Database Connection
mongoose.connect("mongodb+srv://tesorocelina:admin123@zuitt-bootcamp.ycfxf2m.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", {
	// avoids current and future errors while connecting to MongoDB
			useNewUrlParser: true,
			useUnifiedTopology: true,
});

mongoose.connection.once("open", () => console.log("Now connected to cloud database!"));

// Server listening
// will use the defined port number for the app whenever environment variable is available, or use port 4000 if none is defined
// this syntax will allow flexibility when using the application locally oras a hosted app
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));
