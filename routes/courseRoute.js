const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");

const auth = require("../auth");

// router.post("/create", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
// });

// Route for creating a course
router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;

// S39 ACTIVITY START
/*router.post("/create", auth.verify, (req,res) =>
{
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
});*/
// S39 ACTIVITY END

// Route for retrieving all courses
router.get("/all",(req, res) =>
{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

router.get("/active",(req, res) =>
{
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving specific course
router.get("/:courseId",(req, res) =>
{
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating specific course
router.put("/:courseId", (req, res) =>
{
	courseController.updateCourse(req.params,req.body).then(resultFromController => res.send(resultFromController));
});

// S40 ACTIVITY START
// Route for patching specific course as authorized user
router.patch("/:courseId/archive", auth.verify, (req, res) =>
{
	courseController.archiveCourse(req.params,req.body).then(resultFromController => res.send(resultFromController));
});
// S40 ACTIVITY END